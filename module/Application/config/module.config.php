<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\View\Helper;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'index' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/[:competitionId]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                        'competitionId'       => 1
                    ],
                    'constraints' => [
                        'id' => '[1-9]\d*',
                    ],
                ]
            ],
            'division' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/competition/:competitionId/division/:id',
                    'defaults' => [
                        'controller' => Controller\CompetitionController::class,
                        'action'     => 'playDivision'
                    ],
                    'constraints' => [
                        'id' => '[1-9]\d*',
                        'competitionId' => '[1-9]\d*',
                    ],
                ]
            ],
            'playoffs' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/competition/:competitionId/playoffs',
                    'defaults' => [
                        'controller' => Controller\CompetitionController::class,
                        'action'     => 'playoffs'
                    ],
                    'constraints' => [
                        'competitionId' => '[1-9]\d*',
                    ],
                ]
            ],
            'default' => [
                'type' => \Zend\Router\Http\Segment::class,
                'options' => [
                    'route'    => '/[:controller[/:action]]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => Controller\Factory\IndexControllerFactory::class,
            Controller\CompetitionController::class => Controller\Factory\CompetitionControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\MatchService::class => Service\Factory\BasicServiceFactory::class,
            Service\CompetitionService::class => Service\Factory\CompetitionServiceFactory::class,
            Service\CompetitionStageService::class => Service\Factory\CompetitionStageServiceFactory::class,
            Service\DivisionService::class => Service\Factory\DivisionServiceFactory::class,
            Service\PlayoffService::class => Service\Factory\PlayoffServiceFactory::class
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'view_helpers' => [
        'factories' => [
            Helper\MatchParticipantScore::class => InvokableFactory::class,
        ],
        'aliases' => [
            'matchParticipantScore' => Helper\MatchParticipantScore::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity',
                    __DIR__ . '/../src/ValueObject'
                ]
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                    __NAMESPACE__ . '\ValueObject' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ]
];
