<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class MatchParticipantScore extends AbstractHelper
{
    // match, and 2 participants given, so the needed score order would be known
    public function __invoke($match, $participantId)
    {
        $score = '';
        $winnerId = $match->getWinner()->getId();
        if ($participantId === $winnerId) {
            $score .= $match->getScore()->getWinnerScore();
            $score .= ':';
            $score .= $match->getScore()->getLoserScore();
        } else {
            $score .= $match->getScore()->getLoserScore();
            $score .= ':';
            $score .= $match->getScore()->getWinnerScore();
        }

        return htmlspecialchars($score, ENT_QUOTES, 'UTF-8');
    }
}
