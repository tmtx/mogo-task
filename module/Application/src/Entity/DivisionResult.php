<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\ParticipantInterface;
use Application\Entity\CompetitionStageInterface;
use Application\Entity\DivisionResultInterface;

/**
 * Results of the competition
 *
 * @ORM\Entity
 * @ORM\Table(name="division_result")
 */
class DivisionResult implements DivisionResultInterface
{
    /** * @ORM\Id * @ORM\GeneratedValue * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Participant")
     */
    protected $participant;

    /**
     * @ORM\Column(name="points", type="smallint")
     */
    protected $points;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\CompetitionStage", inversedBy="results")
     */
    protected $stage;

    public function __construct(ParticipantInterface $participant, int $points)
    {
        $this->participant = $participant;
        $this->points = $points;
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function setId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function getParticipant(): ParticipantInterface
    {
        return $this->participant;
    }

    /**
     * {@inheritDoc}
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * {@inheritDoc}
     */
    public function getStage(): CompetitionStageInterface
    {
        return $this->stage;
    }

    /**
     * {@inheritDoc}
     */
    public function setStage(CompetitionStageInterface $stage)
    {
        $this->stage = $stage;
    }
}
