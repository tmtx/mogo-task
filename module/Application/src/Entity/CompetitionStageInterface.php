<?php
namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Application\Entity\MatchInterface;
use Application\Entity\ParticipantInterface;
use Application\Entity\CompetitionInterface;
use Application\ValueObject\CompetitionStageRulesInterface;
use Application\ValueObject\StageType;

interface CompetitionStageInterface
{
    /**
     * Will get id for this stage
     */
    public function getId(): int;

    /**
     * Will set id for this stage
     */
    public function setId(int $id);

    /**
     * Will return competition rules
     */
    public function getRules(): CompetitionStageRulesInterface;

    /**
     * Will get competition for this stage
     */
    public function getCompetition(): CompetitionInterface;

    /**
     * Will set competition for this stage
     */
    public function setCompetition(CompetitionInterface $competition);

    /**
     * Will return stage matches
     */
    public function getMatches(): ArrayCollection;
/**
     * Will set stage matches
     */
    public function setMatches(ArrayCollection $matches);

    /**
     * Will add a match to this stage
     */
    public function addMatch(MatchInterface $match);

    /**
     * Will get title of this stage
     */
    public function getTitle(): string;

    /**
     * Will set title for this stage
     */
    public function setTitle(string $title);

    /**
     * Will get type of this stage division|none|playoff
     */
    public function getType(): StageType;

    /**
     * Will set type for this stage division|none|playoff
     */
    public function setType(StageType $type);

    /**
     * Will get index for this stage
     */
    public function getIndex(): int;

    /**
     * Will set index for this stage
     */
    public function setIndex(int $index);

    /**
     * Will get participants of current stage
     */
    public function getParticipants(): ArrayCollection;

    /**
     * Will add participants of current stage
     */
    public function addParticipants(ArrayCollection $participants);

    /**
     * Will get participants of current stage
     */
    public function getAdvancedParticipants(): ArrayCollection;

    /**
     * Will add participants that have advanced to next stage
     */
    public function addAdvancedParticipants(ArrayCollection $participants);
}
