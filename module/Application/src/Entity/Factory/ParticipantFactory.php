<?php
namespace Application\Entity\Factory;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\Participant;

class ParticipantFactory
{
    /**
     * Will return ArrayCollection with Participants, given list of names
     */
    public static function createWithNames(array $names): ArrayCollection
    {
        $participants = new ArrayCollection();
        foreach ($names as $name) {
            $participants->add(new Participant($name));
        }

        return $participants;
    }
}
