<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Application\Entity\CompetitionStage;

use Application\Entity\PlayoffMatchInterface;
use Application\Entity\PlayoffRoundInterface;
use Application\ValueObject\CompetitionStageRulesInterface;
use Application\ValueObject\StageType;

/**
 * Playoff round - type of competition stage (round of 4, round of 8, round of 16 etc.)
 *
 * @ORM\Entity
 */
class PlayoffRound extends CompetitionStage implements PlayoffRoundInterface
{
    /**
     * @ORM\OneToMany(targetEntity="\Application\Entity\PlayoffMatch", mappedBy="stage", cascade={"persist"})
     */
    protected $matches;

    /**
     * @ORM\OneToMany(targetEntity="\Application\Entity\Participant", mappedBy="stages", cascade={"persist"})
     */
    protected $participants;

    public function __construct(CompetitionStageRulesInterface $rules, int $index)
    {
        $this->matches = new ArrayCollection();
        parent::__construct(StageType::PLAYOFF(), $rules, $index);
    }

    /**
     * {@inheritDoc}
     */
    public function addPlayoffMatch(PlayoffMatchInterface $match)
    {
        $this->matches->add($match);
    }
}
