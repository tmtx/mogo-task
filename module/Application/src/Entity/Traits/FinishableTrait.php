<?php
namespace Application\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

trait FinishableTrait
{
    /**
     * @ORM\Column(name="finished_at", type="datetime", nullable=true)
     */
    protected $finishedAt;

    /**
     * @ORM\Column(name="is_finished", type="boolean")
     */
    protected $isFinished = false;

    /**
     * Will return if competition has finished
     */
    public function getFinishedAt(): ?DateTime
    {
        return $this->finishedAt;
    }

    /**
     * Will set if competition has finished
     */
    public function setFinishedAt(?DateTime $finishedAt)
    {
        $this->finishedAt = $finishedAt;
    }

    /**
     * Will return if competition has finished
     */
    public function getIsFinished(): bool
    {
        return $this->isFinished;
    }

    /**
     * Will set if competition has finished
     */
    public function setIsFinished(bool $isFinished)
    {
        $this->isFinished = $isFinished;
    }
}
