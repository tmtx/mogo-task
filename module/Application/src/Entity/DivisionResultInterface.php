<?php
namespace Application\Entity;

use Application\Entity\ParticipantInterface;
use Application\Entity\CompetitionStageInterface;
use Application\Entity\DivisionResultInterface;

interface DivisionResultInterface
{
    /**
     * Will return participant
     */
    public function getParticipant(): ParticipantInterface;

    /**
     * Will return points
     */
    public function getPoints(): int;

    /**
     * Will return competition
     */
    public function getStage(): CompetitionStageInterface;

    /**
     * Will set competition
     */
    public function setStage(CompetitionStageInterface $stage);
}
