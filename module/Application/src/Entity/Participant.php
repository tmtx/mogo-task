<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\ParticipantInterface;
use Application\Entity\CompetitionStageInterface;

/**
 * Competition participant
 *
 * @ORM\Entity
 * @ORM\Table(name="participant")
 */
class Participant implements ParticipantInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="\Application\Entity\CompetitionStage", inversedBy="participants", cascade={"persist"})
     * @ORM\JoinTable(
     *  name="participant_stages",
     *  joinColumns={
     *      @ORM\JoinColumn(name="participant_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="stage_id", referencedColumnName="id")
     *  }
     * )
     */
    protected $stages;

    /**
     * @ORM\ManyToMany(targetEntity="\Application\Entity\Match", inversedBy="participants", cascade={"persist"})
     */
    protected $matches;

    /**
     * @ORM\ManyToMany(targetEntity="\Application\Entity\CompetitionStage", inversedBy="advancedParticipants", cascade={"persist"})
     * @ORM\JoinTable(
     *  name="advanced_participant_stages",
     *  joinColumns={
     *      @ORM\JoinColumn(name="participant_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="stage_id", referencedColumnName="id")
     *  }
     * )
     */
    protected $advancedStages;

    public function __construct(?string $name)
    {
        $this->name = $name;
        $this->matches = new ArrayCollection();
        $this->stages = new ArrayCollection();
        $this->advancedStages = new ArrayCollection();
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritDoc}
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * {@inheritDoc}
     */
    public function addMatch(MatchInterface $match)
    {
        $this->matches->add($match);
    }

    /**
     * {@inheritDoc}
     */
    public function getMatches(): ArrayCollection
    {
        return new ArrayCollection($this->matches->toArray());
    }

    /**
     * {@inheritDoc}
     */
    public function setMatches(ArrayCollection $matches)
    {
        $this->matches = $matches;
    }

    /**
     * {@inheritDoc}
     */
    public function getStages(): ArrayCollection
    {
        return new ArrayCollection($this->stages->toArray());
    }

    /**
     * {@inheritDoc}
     */
    public function addStage(CompetitionStageInterface $stage)
    {
        $this->stages->add($stage);
    }

    /**
     * {@inheritDoc}
     */
    public function getMatchesWithOpponent(ParticipantInterface $opponent): ArrayCollection
    {
        $matches = new ArrayCollection;
        foreach ($this->getMatches() as $match) {
            foreach ($match->getParticipants() as $p) {
                if ($p->getId() === $opponent->getId()) {
                    $matches->add($match);
                    break;
                }
            }
        }

        return $matches;
    }

    /**
     * {@inheritDoc}
     */
    public function addAdvancedStage(CompetitionStageInterface $stage)
    {
        $this->advancedStages->add($stage);
    }
}
