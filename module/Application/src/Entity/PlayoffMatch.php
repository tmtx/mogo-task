<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Application\Entity\Match;

/**
 * Basic competition match
 * TODO: perhaps better to use only one class - Match with field `type`
 *
 * @ORM\Entity
 */
class PlayoffMatch extends Match
{
    /**
     * @ORM\Column(name="match_key", type="integer")
     */
    protected $matchKey;

    /**
     * @ORM\Column(name="is_consolation", type="boolean")
     */
    protected $isConsolation;

    public function __construct(ArrayCollection $participants) {
        $this->isConsolation = false;
        parent::__construct($participants);
    }

    /**
     * {@inheritDoc}
     */
    public function getNextMatchKey(): int
    {
        return $this->matchKey;
    }

    /**
     * {@inheritDoc}
     */
    public function setNextMatchKey(int $key)
    {
        $this->matchKey = $key;
    }

    /**
     * {@inheritDoc}
     */
    public function getIsConsolation(): bool
    {
        return $this->isConsolation;
    }

    /**
     * {@inheritDoc}
     */
    public function setIsConsolation(bool $isConsolation)
    {
        $this->isConsolation = $isConsolation;
    }
}
