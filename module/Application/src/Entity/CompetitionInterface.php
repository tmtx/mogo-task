<?php
namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\ParticipantResultInterface;
use Application\ValueObject\CompetitionRulesInterface;
use Application\Entity\CompetitionStageInterface;

interface CompetitionInterface
{
    /**
     * Will get id for this stage
     */
    public function getId(): int;

    /**
     * Will set id for this stage
     */
    public function setId(int $id);

    /**
     * Will return competition rules
     */
    public function getRules(): CompetitionRulesInterface;

    /**
     * Will return all competition stages
     */
    public function getStages(): ArrayCollection;

    /**
     * Will add an additional competition stage
     */
    public function addStage(CompetitionStageInterface $stage);

    /**
     * Will return results of competitions
     */
    public function getResults(): ArrayCollection;

    /**
     * Will set if competition has finished
     */
    public function addResult(CompetitionResultInterface $result);
}
