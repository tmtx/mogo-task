<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Application\Entity\MatchInterface;
use Application\Entity\ParticipantInterface;
use Application\ValueObject\MatchScoreInterface;
use Application\Entity\CompetitionStageInterface;

use Application\Entity\Traits\FinishableTrait;

/**
 * Basic competition match
 *
 * @ORM\Entity
 * @ORM\Table(name="competition_match")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"match" = "Match", "playoffMatch"="PlayoffMatch"})
 */
class Match implements MatchInterface
{
    use FinishableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="\Application\Entity\Participant")
     */
    protected $participants;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\CompetitionStage", inversedBy="matches")
     */
    protected $stage;

    /**
     *@ORM\Embedded(class="\Application\ValueObject\MatchScore", columnPrefix = "result_")
     */
    protected $score;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Participant")
     * @ORM\JoinColumn(name="winner_id", referencedColumnName="id")
     */
    protected $winner;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Participant")
     * @ORM\JoinColumn(name="loser_id", referencedColumnName="id")
     */
    protected $loser;

    public function __construct(ArrayCollection $participants)
    {
        $this->participants = $participants ?? new ArrayCollection();
        foreach ($participants as $p) {
            $p->addMatch($this);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * {@inheritDoc}
     */
    public function getParticipants(): ArrayCollection
    {
        return new ArrayCollection($this->participants->toArray());
    }

    /**
     * {@inheritDoc}
     */
    public function setParticipants(ArrayCollection $participants)
    {
        $this->participants = $participants;
    }

    /**
     * {@inheritDoc}
     */
    public function setScore(MatchScoreInterface $matchScore)
    {
        $this->score = $matchScore;
    }

    /**
     * {@inheritDoc}
     */
    public function getScore(): ?MatchScoreInterface
    {
        return $this->score;
    }

    /**
     * {@inheritDoc}
     */
    public function setStage(CompetitionStageInterface $stage)
    {
        $this->stage = $stage;
    }

    /**
     * {@inheritDoc}
     */
    public function getStage(): CompetitionStageInterface
    {
        return $this->stage;
    }

    /**
     * {@inheritDoc}
     */
    public function setWinner(ParticipantInterface $winner)
    {
        $this->winner = $winner;
    }

    /**
     * {@inheritDoc}
     */
    public function getWinner(): ?ParticipantInterface
    {
        return $this->winner;
    }

    /**
     * {@inheritDoc}
     */
    public function setLoser(ParticipantInterface $loser)
    {
        $this->loser = $loser;
    }

    /**
     * {@inheritDoc}
     */
    public function getLoser(): ?ParticipantInterface
    {
        return $this->loser;
    }

    /**
     * {@inheritDoc}
     */
    public function getParticipantScores(): ArrayCollection {
        $scores = new ArrayCollection();
        if (! $this->isFinished) {
            return $scores;
        }
        $scores->set($this->getWinner()->getId(), $this->getScore()->getWinnerScore());
        $scores->set($this->getLoser()->getId(), $this->getScore()->getLoserScore());
        return $scores;
    }
}
