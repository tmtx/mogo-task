<?php
namespace Application\Entity;

use Application\Entity\MatchInterface;

interface PlayoffMatchInterface extends MatchInterface
{
    /**
     * Will get next match key.
     * This determines which place in the bracket is this match
     */
    public function getNextMatchKey(): int;

    /**
     * Will set next match key.
     */
    public function setNextMatchKey(int $key);

    /**
     * Will get if this is match for 3rd place
     */
    public function getIsConsolation(): bool;

    /**
     * Will set if this is match for 3rd place
     */
    public function setIsConsolation(bool $isConsolation);
}
