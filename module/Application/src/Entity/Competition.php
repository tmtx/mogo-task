<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\CompetitionResultInterface;
use Application\ValueObject\CompetitionRulesInterface;
use Application\Entity\CompetitionStageInterface;
use Application\Entity\CompetitionInterface;
use Application\Exception\RulesViolatedException;
use Application\ValueObject\StageType;

use Application\Entity\Traits\FinishableTrait;

/**
 * Competitions
 *
 * @ORM\Entity
 * @ORM\Table(name="competition")
 */
class Competition implements CompetitionInterface
{
    use FinishableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     *@ORM\Embedded(class="\Application\ValueObject\CompetitionRules", columnPrefix = "rules_")
     */
    protected $rules;

    /**
     * @ORM\OneToMany(targetEntity="\Application\Entity\CompetitionStage", mappedBy="competition", cascade={"persist"})
     */
    protected $stages;

    /**
     * @ORM\OneToMany(targetEntity="\Application\Entity\CompetitionResult", mappedBy="competition")
     */
    protected $results;

    public function __construct(CompetitionRulesInterface $rules)
    {
        $this->rules = $rules;
        $this->matches = new ArrayCollection();
        $this->participants = new ArrayCollection();
        $this->stages = new ArrayCollection();
        $this->advancedParticipants = new ArrayCollection();
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * {@inheritDoc}
     */
    public function getRules(): CompetitionRulesInterface
    {
        return $this->rules;
    }

    /**
     * {@inheritDoc}
     */
    public function getStages(): ArrayCollection
    {
        return new ArrayCollection($this->stages->toArray());
    }

    /**
     * {@inheritDoc}
     */
    public function addStage(CompetitionStageInterface $stage)
    {
        $this->stages->add($stage);
        $stage->setCompetition($this);
        $participantCount = 0;
        $divisionCount = 0;
        $maxParticipantCount = $this->rules->getMaxParticipantCount();
        $allowedDivisionCount = $this->rules->getDivisionCount();
        $playoffStages = $this->stages->filter(function($s) {
            if ($s->getType() === StageType::PLAYOFF()) {
                return true;
            }
            return false;
        });
        $divisionStages = $this->stages->filter(function($s) {
            if ($s->getType() === StageType::DIVISION()) {
                return true;
            }
            return false;
        });
        if ($playoffStages->count() > 0) {
            $maxIndex = 0;
            foreach ($playoffStages as $s) {
                // get participant count of last playoff stage
                if ($s->getIndex() < $maxIndex) {
                    continue;
                }
                $participantCount = $s->getParticipants()->count();
                $maxIndex = $s->getIndex();
            }
        } else {
            foreach ($divisionStages as $s) {
                $participantCount += $s->getParticipants()->count();
                $divisionCount += 1;
            }
        }
        if ($participantCount > $maxParticipantCount) {
            throw new RulesViolatedException(sprintf(
                "Competition participant count %d exceeds maximum of %d",
                $participantCount,
                $maxParticipantCount
            ));
        }
        if ($allowedDivisionCount > $this->rules->getDivisionCount()) {
            throw new RulesViolatedException(sprintf(
                "Competitions division count %d exceeds maximum of %d",
                $divisionCount,
                $allowedDivisionCount
            ));
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getResults(): ArrayCollection
    {
        return $this->results;
    }

    /**
     * {@inheritDoc}
     */
    public function addResult(CompetitionResultInterface $result)
    {
        $this->results->add($result);
    }
}
