<?php
namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\CompetitionStageInterface;

interface ParticipantInterface
{
    /**
     * Will get id for this stage
     */
    public function getId(): ?int;

    /**
     * Will set id for this stage
     */
    public function setId(int $id);

    /**
     * Will return participant name
     */
    public function getName(): string;

    /**
     * Will set participant name
     */
    public function setName(string $name);

    /**
     * Will add match to participant
     */
    public function addMatch(MatchInterface $match);

    /**
     * Will get matches a participant has played
     */
    public function getMatches(): ArrayCollection;

    /**
     * Will set matches a participant has played
     */
    public function setMatches(ArrayCollection $matches);

    /**
     * Will return competition stages participant has participated in
     */
    public function getStages(): ArrayCollection;

    /**
     * Will add competition stage participant has participated in
     */
    public function addStage(CompetitionStageInterface $stage);

    /**
     * Will add competition stages participant has advnaced from
     */
    public function addAdvancedStage(CompetitionStageInterface $stage);

    /**
     * Will return matches with particular opponent
     */
    public function getMatchesWithOpponent(ParticipantInterface $opponent): ArrayCollection;
}
