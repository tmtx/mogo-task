<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\CompetitionStage;

/**
  * Division - stage in which each participant plays with all others
  *
  * @ORM\Entity
  */
class Division extends CompetitionStage
{
}
