<?php
namespace Application\Entity;

use Application\Entity\CompetitionStageInterface;
use Application\Entity\PlayoffMatchInterface;

interface PlayoffRoundInterface extends CompetitionStageInterface
{
    /**
     * Will add playoff match to a round (round of 8, round of 4, etc.)
     */
    public function addPlayoffMatch(PlayoffMatchInterface $match);
}
