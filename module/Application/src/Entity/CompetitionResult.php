<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\ParticipantInterface;
use Application\Entity\CompetitionResultInterface;

/**
 * Results of the competition
 *
 * @ORM\Entity
 * @ORM\Table(name="competition_result")
 */
class CompetitionResult implements CompetitionResultInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Participant")
     */
    protected $participant;

    /**
     * @ORM\Column(name="place", type="smallint")
     */
    protected $place;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Competition", inversedBy="results")
     * @ORM\JoinColumn(name="competition_id", referencedColumnName="id")
     */
    protected $competition;

    public function __construct(ParticipantInterface $participant, int $place)
    {
        $this->participant = $participant;
        $this->place = $place;
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function setId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function getParticipant(): ParticipantInterface
    {
        return $this->participant;
    }

    /**
     * {@inheritDoc}
     */
    public function getPlace(): int
    {
        return $this->place;
    }

    /**
     * {@inheritDoc}
     */
    public function getCompetition(): CompetitionInterface
    {
        return $this->competition;
    }

    /**
     * {@inheritDoc}
     */
    public function setCompetition(CompetitionInterface $competition)
    {
        $this->competition = $competition;
    }
}
