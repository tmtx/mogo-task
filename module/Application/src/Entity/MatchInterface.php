<?php
namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Application\Entity\ParticipantInterface;
use Application\ValueObject\MatchScoreInterface;
use Application\Entity\CompetitionStageInterface;

interface MatchInterface
{
    /**
     * Will get id for this stage
     */
    public function getId(): int;

    /**
     * Will set id for this stage
     */
    public function setId(int $id);

    /**
     * Will return participants of the match
     */
    public function getParticipants(): ArrayCollection;

    /**
     * Will set participants of the match
     */
    public function setParticipants(ArrayCollection $participants);

    /**
     * Will set result of the match
     */
    public function setScore(MatchScoreInterface $matchScore);

    /**
     * Will return score of the match
     */
    public function getScore(): ?MatchScoreInterface;

    /**
     * Will set competition match belongs to
     */
    public function setStage(CompetitionStageInterface $stage);

    /**
     * Will get competition match belongs to
     */
    public function getStage(): CompetitionStageInterface;

    /**
     * Will return winner of match
     */
    public function getWinner(): ?ParticipantInterface;

    /**
     * Will set winner of match
     */
    public function setWinner(ParticipantInterface $participant);

    /**
     * Will return loser of match
     */
    public function getLoser(): ?ParticipantInterface;

    /**
     * Will set loser of match
     */
    public function setLoser(ParticipantInterface $participant);
    /**
     * Will return collection of scores for this match
     */
    public function getParticipantScores(): ArrayCollection;
}
