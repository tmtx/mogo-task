<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Application\Entity\MatchInterface;
use Application\Entity\ParticipantInterface;
use Application\Entity\CompetitionStageInterface;
use Application\Entity\CompetitionInterface;
use Application\Exception\RulesViolatedException;
use Application\ValueObject\CompetitionStageRulesInterface;
use Application\ValueObject\StageType;

use Application\Entity\Traits\FinishableTrait;

/**
 * Competition stage
 *
 * @ORM\Entity
 * @ORM\Table(name="competition_stage")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"stage" = "CompetitionStage", "playoffRound"="PlayoffRound", "division"="Division"})
 */
class CompetitionStage implements CompetitionStageInterface
{
    use FinishableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="type", type="string")
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Competition", inversedBy="stages", cascade={"persist"})
     */
    protected $competition;

    /**
     * @ORM\OneToMany(targetEntity="\Application\Entity\Match", mappedBy="stage", cascade={"persist"})
     */
    protected $matches;

    /**
     * @ORM\Column(name="title", type="string")
     */
    protected $title;

    /**
     * @ORM\Column(name="stage_index", type="smallint")
     */
    protected $index;

    /**
     * @ORM\ManyToMany(targetEntity="\Application\Entity\Participant", mappedBy="stages", cascade={"persist"})
     */
    protected $participants;

    /**
     * @ORM\ManyToMany(targetEntity="\Application\Entity\Participant", mappedBy="advancedStages", cascade={"persist"})
     */
    protected $advancedParticipants;

    /**
     *@ORM\Embedded(class="\Application\ValueObject\CompetitionStageRules", columnPrefix = "rules_")
     */
    private $rules;

    public function __construct(StageType $type, CompetitionStageRulesInterface $rules, ?int $index)
    {
        $this->rules = $rules;
        $this->setIndex($index);
        $this->matches = new ArrayCollection();
        $this->participants = new ArrayCollection();
        $this->advancedParticipants = new ArrayCollection();
        $this->setType($type);
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * {@inheritDoc}
     */
    public function getType(): StageType
    {
        if (empty($this->type)) {
            return StageType::NONE();
        }

        if (! $this->type instanceof StageType) {
            $v = StageType::search(strtolower($this->type));
            return new StageType(strtolower($v));
        }

        return $this->type;
    }

    /**
     * {@inheritDoc}
     */
    public function setType(StageType $type)
    {
        $this->type = $type;
    }

    /**
     * {@inheritDoc}
     */
    public function getRules(): CompetitionStageRulesInterface
    {
        return $this->rules;
    }

    /**
     * {@inheritDoc}
     */
    public function getCompetition(): CompetitionInterface
    {
        return $this->competition;
    }

    /**
     * {@inheritDoc}
     */
    public function setCompetition(CompetitionInterface $competition)
    {
        $this->competition = $competition;
    }

    /**
     * {@inheritDoc}
     */
    public function getMatches(): ArrayCollection
    {
        return new ArrayCollection($this->matches->toArray());
    }

    /**
     * {@inheritDoc}
     */
    public function setMatches(ArrayCollection $matches)
    {
        foreach ($matches as $match) {
            $match->setStage($this);
        }
        return $this->matches = $matches;
    }

    /**
     * {@inheritDoc}
     */
    public function addMatch(MatchInterface $match)
    {
        $match->setStage($this);
        $this->matches->add($match);
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * {@inheritDoc}
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * {@inheritDoc}
     */
    public function setIndex(int $index)
    {
        $this->index = $index;
    }

    /**
     * {@inheritDoc}
     */
    public function getParticipants(): ArrayCollection
    {
        return new ArrayCollection($this->participants->toArray());
    }

    /**
     * {@inheritDoc}
     */
    public function addParticipants(ArrayCollection $participants)
    {
        $oldParticipants = $this->getParticipants()->toArray();
        foreach($participants as $p) {
            $this->participants->add($p);
        }
        if ($this->participants->count() > $this->rules->getMaxParticipantCount()) {
            throw new RulesViolatedException(sprintf(
                "Can't add %d participants. Maximum is %d for this competition",
                $participantCount,
                $maxParticipantCount
            ));
        }
        foreach ($this->getParticipants() as $participant) {
            $participant->addStage($this);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAdvancedParticipants(): ArrayCollection
    {
        if (empty($this->advancedParticipants)) {
            return new ArrayCollection();
        }
        return new ArrayCollection($this->advancedParticipants->toArray());
    }

    /**
     * {@inheritDoc}
     */
    public function addAdvancedParticipants(ArrayCollection $participants)
    {
        foreach ($participants as $p) {
            $this->advancedParticipants->add($p);
        }
        foreach ($this->getAdvancedParticipants() as $participant) {
            $participant->addAdvancedStage($this);
        }
    }
}
