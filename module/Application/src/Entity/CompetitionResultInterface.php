<?php
namespace Application\Entity;

use Application\Entity\ParticipantInterface;
use Application\Entity\CompetitionInterface;

interface CompetitionResultInterface
{
    /**
     * Will return participant
     */
    public function getParticipant(): ParticipantInterface;

    /**
     * Will return points
     */
    public function getPlace(): int;

    /**
     * Will return competition
     */
    public function getCompetition(): CompetitionInterface;

    /**
     * Will set competition
     */
    public function setCompetition(CompetitionInterface $competition);
}
