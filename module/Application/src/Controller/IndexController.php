<?php
namespace Application\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Competition;
use Application\Entity\PlayoffRound;

class IndexController extends AbstractActionController
{
    private $entityManager;
    private $competitionService;
    private $divisionService;

    public function __construct($entityManager, $competitionService, $divisionService)
    {
        $this->entityManager = $entityManager;
        $this->competitionService = $competitionService;
        $this->divisionService = $divisionService;
    }

    public function indexAction()
    {
        $competitionId = $this->params()->fromRoute('competitionId', null);

        $divisionId = $this->params()->fromQuery('divisionId', null);
        $competition = $this->competitionService->getOrCreate($competitionId);

        $viewData = new ViewModel([
            'competitionId' => $competition->getId()
        ]);

        $divisionsData = new ArrayCollection();
        if (! empty($competition)) {
            $divisions = $this->competitionService->getDivisions($competition);
            foreach ($divisions as $division) {
                $participants = $division->getParticipants()->toArray();
                usort($participants, function($a, $b) {
                    return $a->getName() > $b->getName();
                });
                $divisionsData->set($division->getIndex(), [
                    'participants' => $participants,
                    'title' => $division->getTitle(),
                    'results' => $this->divisionService->getResults($division),
                    'topResults' => $this->divisionService->getTopResults($division)
                ]);
            }
            $playoffRounds = $this->competitionService->getPlayoffRounds($competition);
            $viewData->setVariable('playoffRounds', $playoffRounds);
            $competitionStandings = $this->competitionService->getCompetitionResults($competition);
            $viewData->setVariable('competitionStandings', $competitionStandings);
        }
        $iterator = $divisionsData->getIterator();
        $iterator->ksort();
        $divisionsData = new ArrayCollection(iterator_to_array($iterator));
        $viewData->setVariable('divisionsData', $divisionsData);

        return $viewData;
    }
}
