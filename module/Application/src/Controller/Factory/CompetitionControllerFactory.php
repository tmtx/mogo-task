<?php

namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\CompetitionController;
use Application\Service\CompetitionService;

class CompetitionControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $competitionService = $container->get(CompetitionService::class);

        return new CompetitionController($entityManager, $competitionService);
    }
}
