<?php

namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\IndexController;
use Application\Service\CompetitionService;
use Application\Service\DivisionService;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $competitionService = $container->get(CompetitionService::class);
        $divisionService = $container->get(DivisionService::class);

        return new IndexController($entityManager, $competitionService, $divisionService);
    }
}
