<?php
namespace Application\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Zend\Mvc\Controller\AbstractActionController;

use Application\Entity\Factory\ParticipantFactory;
use Application\Entity\Competition;

class CompetitionController extends AbstractActionController
{
    private $entityManager;
    private $competitionService;

    public function __construct($entityManager, $competitionService)
    {
        $this->entityManager = $entityManager;
        $this->competitionService = $competitionService;
    }

    public function indexAction()
    {
        return new ViewModel();
    }

    public function playDivisionAction()
    {
        $competitionId = $this->params()->fromRoute('competitionId', 1);
        $id = $this->params()->fromRoute('id', 0);
        if (! in_array($id, [1, 2])) {
            throw new \Exception('ID must be 1 or 2 for this action');
        }

        $competition = $this->competitionService->getOrCreate($competitionId);
        $this->entityManager->persist($competition);

        $names = [];
        foreach (range('A', 'Z') as $char) {
            if (count($names) === 16) {
                break;
            }
            $names[] = $char;
        }

        if (intval($id) === 1) {
            $names = array_slice($names, 0, 8);
        } else {
            $names = array_slice($names, 8);
        }

        $participants = ParticipantFactory::createWithNames($names);

        $idTitles = [
            1 => 'Division A',
            2 => 'Division B',
        ];
        $divisionOptions = [
            'max_participant_count' => 8,
            'advances_count' => 4,
            'title' => $idTitles[$id],
            'index' => $id
        ];

        $this->competitionService->runDivisionStage($id, $competition, $participants, $divisionOptions);
        $this->entityManager->persist($competition);
        $this->entityManager->flush();
        return $this->redirect()->toRoute('index', [
            'competitionId' => $competition->getId()
        ]);
    }

    public function playoffsAction()
    {
        $competitionId = $this->params()->fromRoute('competitionId', null);
        if (empty($competitionId)) {
            return $this->redirect()->toRoute('index');
        }
        $competition = $this->entityManager->getRepository(Competition::class)->findOneById($competitionId);
        $this->competitionService->runPlayoffs($competition);

        return $this->redirect()->toRoute('index', [
            'competitionId' => $competition->getId()
        ]);
    }
}
