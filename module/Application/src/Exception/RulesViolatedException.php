<?php
namespace Application\Exception;

use Exception;

/**
 * Competition rules have been violated
 */
class RulesViolatedException extends Exception
{
}
