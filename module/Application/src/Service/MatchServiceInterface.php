<?php
namespace Application\Service;

use Application\Entity\MatchInterface;

interface MatchServiceInterface
{
    /**
     * Will play match
     */
    public function play(MatchInterface $match);
}
