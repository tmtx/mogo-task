<?php
namespace Application\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\CompetitionStageService;
use Application\Service\MatchService;

/**
 * Main competition service
 */
class CompetitionStageServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $matchService = $container->get(MatchService::class);

        return new CompetitionStageService(
            $entityManager,
            $matchService
        );
    }
}
