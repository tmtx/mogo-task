<?php
namespace Application\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\CompetitionStageService;
use Application\Service\PlayoffService;
use Application\Service\DivisionService;

/**
 * Main competition service
 */
class PlayoffServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $competitionStageService = $container->get(CompetitionStageService::class);
        $divisionService = $container->get(DivisionService::class);

        return new PlayoffService(
            $entityManager,
            $competitionStageService,
            $divisionService
        );
    }
}
