<?php
namespace Application\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\CompetitionService;
use Application\Service\CompetitionStageService;
use Application\Service\DivisionService;
use Application\Service\PlayoffService;

/**
 * Main competition service
 */
class CompetitionServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $competitionStageService = $container->get(CompetitionStageService::class);
        $divisionService = $container->get(DivisionService::class);
        $playoffStageService = $container->get(PlayoffService::class);

        return new CompetitionService(
            $entityManager,
            $competitionStageService,
            $divisionService,
            $playoffStageService
        );
    }
}
