<?php
namespace Application\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\CompetitionStage;
use Application\Entity\PlayoffRoundInterface;
use Application\Entity\PlayoffRound;
use Application\Entity\PlayoffMatch;
use Application\Entity\CompetitionStageInterface;
use Application\ValueObject\CompetitionStageRules;
use Application\Service\PlayoffServiceInterface;

class PlayoffService implements PlayoffServiceInterface
{
    /**
     * Entity manager.
     */
    private $entityManager;

    /**
     * Competition stage service
     */
    private $competitionStageService;

    /**
     * CompetitionStageInterface service
     */
    private $divisionService;

    public function __construct($entityManager, $competitionStageService, $divisionService)
    {
        $this->entityManager = $entityManager;
        $this->competitionStageService = $competitionStageService;
        $this->divisionService = $divisionService;
    }

    /**
     * {@inheritDoc}
     */
    public function createNextRoundMatches(PlayoffRoundInterface $round): ArrayCollection
    {
        // ensure correct order
        $round->getMatches()->first();
        $nextMatchParticipants = new ArrayCollection();
        $matches = new ArrayCollection();
        $nextMatchKey = 0;
        foreach ($round->getMatches() as $match) {
            if (! $match->getIsFinished()) {
                continue;
            }
            $newMatchKey = $match->getNextMatchKey();
            if ($nextMatchParticipants->containsKey($newMatchKey)) {
                // make new match, if already have one player for new key
                $participants = new ArrayCollection([
                    $nextMatchParticipants->get($newMatchKey),
                    $match->getWinner()

                ]);
                $nextMatch = new PlayoffMatch($participants);
                $nextMatch->setNextMatchKey($nextMatchKey);
                if (count($matches) % 2 == 1) {
                    $nextMatchKey += 1;
                }
                $matches->add($nextMatch);
            }
            $nextMatchParticipants->set($match->getNextMatchKey(), $match->getWinner());
        }
        $matchesIterator = $matches->getIterator();
        $matchesIterator->uasort(function($a, $b) {
            return $a->getNextMatchKey() > $b->getNextMatchKey();
        });
        $matches = new ArrayCollection(iterator_to_array($matchesIterator));

        return $matches;
    }

    /**
     * {@inheritDoc}
     */
    public function runAllRounds(ArrayCollection $rounds)
    {
        foreach ($rounds as $round) {
            // first round will already have matches
            if ($round->getMatches()->count() === 0) {
                $matches = $this->createNextRoundMatches($prevRound);
                $round->setMatches($matches);
            }
            if (
                ($rounds->last()->getIndex() == $round->getIndex()) &&
                $prevRound->getMatches()->count() == 2
            ) {
                // 3rd place matches
                $consolationParticipants = new ArrayCollection();
                foreach ($prevRound->getMatches() as $match) {
                    $consolationParticipants->add($match->getLoser());
                }
                $consMatch = new PlayoffMatch($consolationParticipants);
                $consMatch->setIsConsolation(true);
                $round->addMatch($consMatch);
            }
            $this->competitionStageService->playStageMatches($round);
            $round->setIsFinished(true);

            $prevRound = $round;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function createRoundsFromDivisions(CompetitionStageInterface $divisionOne, CompetitionStageInterface $divisionTwo): ArrayCollection
    {
        $rounds = new ArrayCollection();
        $initialRound = $this->initialRoundFromDivisionResults($divisionOne, $divisionTwo);
        $maxParticipants = floor(($divisionOne->getAdvancedParticipants()->count() +
                                  $divisionTwo->getAdvancedParticipants()->count()) / 2);
        $rounds->add($initialRound);
        $index = $initialRound->getIndex() + 1;

        while ($maxParticipants > 1) {
            $round = new PlayoffRound(new CompetitionStageRules([
                'max_participants' => $maxParticipants
            ]), $index);
            $round->setTitle('Round of ' . $maxParticipants);
            $index += 1;
            $maxParticipants = ceil($maxParticipants / 2);
            $this->entityManager->persist($round);
            $rounds->add($round);
        }
        $this->entityManager->flush();

        return $rounds;
    }

    /**
     * {@inheritDoc}
     */
    public function initialRoundFromDivisionResults(CompetitionStageInterface $divisionOne, CompetitionStageInterface $divisionTwo): PlayoffRoundInterface
    {
        $topResultsDivOne = $this->divisionService->getTopResults($divisionOne);
        $topResultsDivTwo = $this->divisionService->getTopResults($divisionTwo);
        $totalResultCount = $topResultsDivOne->count() + $topResultsDivTwo->count();

        $matches = new ArrayCollection();
        $nextMatchKey = 0;
        $topResultsDivOne->first();
        $topResultsDivTwo->first();
        // matches up highest division one participant and lowest division 2
        for ($offset = 0; $offset < floor($totalResultCount / 2); $offset += 1) {
            $resultOne = $topResultsDivOne->get($offset);
            $resultTwo = $topResultsDivTwo->get($topResultsDivTwo->count() - 1 - $offset);
            $participants = new ArrayCollection([
                $resultOne->getParticipant(),
                $resultTwo->getParticipant()
            ]);
            $match = new PlayoffMatch($participants);
            $match->setNextMatchKey($nextMatchKey);
            $matches->add($match);
            // TODO: implement smarter way to calculate next matchup
            if ($offset % 2 == 1) {
                $nextMatchKey += 1;
            }
        }

        $round = new PlayoffRound(new CompetitionStageRules([
            'max_participants' => $totalResultCount
        ]), 1);
        $round->setTitle('Round of ' . $totalResultCount);
        $round->setMatches($matches);

        $this->entityManager->persist($round);
        $this->entityManager->flush();

        return $round;
    }
}
