<?php
namespace Application\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\CompetitionStageInterface;
use Application\ValueObject\StageType;

interface CompetitionStageServiceInterface
{
    /**
     * Will make matches, so that each team plays with every other team
     */
    public function makeRoundRobinMatches(CompetitionStageInterface $competitionStage);

    /**
     * Will play all matches for competition stage
     */
    public function playStageMatches(CompetitionStageInterface $stage);

    /**
     * Will return all advanced participants from multiple stages
     */
    public function getAdvancedFromStages(ArrayCollection $stages): ArrayCollection;

    /**
     * Will create competition stage participants and options
     */
    public function createCompetitionStage(
        StageType $type,
        ArrayCollection $participants,
        array $options
    ): CompetitionStageInterface;
}
