<?php
namespace Application\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\CompetitionStageInterface;
use Application\Entity\CompetitionStage;
use Application\Entity\Division;
use Application\Entity\Match;
use Application\Entity\PlayoffRoundInterface;
use Application\Entity\PlayoffRound;
use Application\ValueObject\CompetitionStageRules;
use Application\ValueObject\StageType;
use Application\Service\CompetitionStageServiceInterface;

class CompetitionStageService implements CompetitionStageServiceInterface
{
    /**
     * Entity manager.
     */
    private $entityManager;

    /**
     * Match service
     */
    private $matchService;

    public function __construct($entityManager, $matchService)
    {
        $this->entityManager = $entityManager;
        $this->matchService = $matchService;
    }

    /**
     * {@inheritDoc}
     */
    public function makeRoundRobinMatches(CompetitionStageInterface $stage)
    {
        $matchHashes = [];
        foreach ($stage->getParticipants() as $participant) {
            foreach ($stage->getParticipants() as $opponent) {
                $participantName = $participant->getName();
                $opponentName = $opponent->getName();
                $participantNames = [$participantName, $opponentName];
                sort($participantNames);
                $matchHash = sha1(implode("", $participantNames).$stage->getId());
                if (
                    ($participantName === $opponentName) ||
                    in_array($matchHash, $matchHashes)
                ) {
                    continue;
                }
                $match = new Match(new ArrayCollection([$participant, $opponent]));
                $stage->addMatch($match);
                $matchHashes[] = $matchHash;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function playStageMatches(CompetitionStageInterface $stage)
    {
        $stage->getMatches()->first();
        foreach ($stage->getMatches() as $match) {
            $this->matchService->play($match);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAdvancedFromStages(ArrayCollection $stages): ArrayCollection
    {
        $allAdvanced = new ArrayCollection();
        foreach ($stages as $stage) {
            $advanced = $stage->getAdvancedParticipants();
            $allAdvanced = array_merge(
                $allAdvanced->toArray(),
                $advanced->toArray()
            );
        }

        return $allAdvanced;
    }

    /**
     * {@inheritDoc}
     */
    public function createCompetitionStage(StageType $type, ArrayCollection $participants, array $options): CompetitionStageInterface
    {
        $competitionStage = new CompetitionStage($type, new CompetitionStageRules($options), $options['index'] ?? 0);
        $competitionStage->addParticipants($participants);
        if (isset($options['title'])) {
            $competitionStage->setTitle($options['title']);
        }

        return $competitionStage;
    }
}
