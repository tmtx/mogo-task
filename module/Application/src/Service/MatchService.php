<?php
namespace Application\Service;

use Application\Entity\MatchInterface;
use Application\ValueObject\MatchScore;

class MatchService implements MatchServiceInterface
{
    /**
     * Entity manager.
     */
    private $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     */
    public function play(MatchInterface $match)
    {
        $result = new MatchScore(1, 0);
        $participants = $match->getParticipants();
        $winnerIndex = rand(0, 1);
        $loserIndex = intval(!$winnerIndex);
        $match->setWinner($participants->get($winnerIndex));
        $match->setLoser($participants->get($loserIndex));
        $match->setScore($result);
        $match->setIsFinished(true);
    }
}
