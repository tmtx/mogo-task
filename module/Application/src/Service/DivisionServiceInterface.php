<?php
namespace Application\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\CompetitionStageInterface;
use Application\Entity\DivisionResultInterface;

interface DivisionServiceInterface
{
    /**
     * Will play all division matches
     */
    public function playDivisionMatches(CompetitionStageInterface $division);

    /**
     * Will return results for competition stage sorted from top to bottom
     */
    public function getTopResults(CompetitionStageInterface $stage): ArrayCollection;

    /**
     * Will return current result after playing division stage
     */
    public function getResults(CompetitionStageInterface $stage): ArrayCollection;
}
