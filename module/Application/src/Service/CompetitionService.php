<?php
namespace Application\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\CompetitionInterface;
use Application\Service\CompetitionServiceInterface;
use Application\Entity\Competition;
use Application\Entity\CompetitionStage;
use Application\Entity\Participant;
use Application\Entity\CompetitionResult;
use Application\ValueObject\CompetitionRules;
use Application\ValueObject\StageType;

class CompetitionService implements CompetitionServiceInterface
{
    /**
     * Entity manager
     */
    private $entityManager;

    /**
     * Competition stage
     */
    private $competitionStageService;

    /**
     * Division service
     */
    private $divisionService;

    /**
     * Playoff stage service
     */
    private $playoffService;

    public function __construct(
        $entityManager,
        $competitionStageService,
        $divisionService,
        $playoffService
    ) {
        $this->entityManager = $entityManager;
        $this->competitionStageService = $competitionStageService;
        $this->divisionService = $divisionService;
        $this->playoffService = $playoffService;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrCreate(?int $id): CompetitionInterface
    {
        if ($id) {
            $competition = $this->entityManager->getRepository(Competition::class)->findOneById($id);
            if (! empty($competition)) {
                return $competition;
            }
        }

        $competition = new Competition(new CompetitionRules([
            'division_count' => 2,
            'max_participant_count' => 16
        ]));
        $this->entityManager->persist($competition);
        $this->entityManager->flush();

        return $competition;
    }

    /**
     * {@inheritDoc}
     */
    public function getDivisions(CompetitionInterface $competition): ArrayCollection
    {
        $divisions = new ArrayCollection();
        foreach ($competition->getStages() as $stage) {
            if ($stage->getType() == StageType::DIVISION()) {
                $divisions->add($stage);
            }
        }

        return $divisions;
    }

    /**
     * {@inheritDoc}
     */
    public function getPlayoffRounds(CompetitionInterface $competition): ArrayCollection
    {
        $playoffRounds = new ArrayCollection();
        foreach ($competition->getStages() as $stage) {
            if ($stage->getType() == StageType::PLAYOFF()) {
                $playoffRounds->add($stage);
            }
        }

        return $playoffRounds;
    }

    /**
     * {@inheritDoc}
     */
    public function removeDivisions(CompetitionInterface $competition)
    {
        foreach ($this->getDivisions($competition) as $stage) {
            $this->entityManager->remove($stage);
        }
    }

    /**
     * {@inheritDoc}
     * TODO: refactor
     */
    public function runDivisionStage(
        int $id,
        CompetitionInterface $competition,
        ArrayCollection $participants,
        array $options
    ) {
        // TODO: think what to do about division type
        $division = $this->competitionStageService->createCompetitionStage(StageType::DIVISION(), $participants, $options);
        $this->entityManager->persist($division);
        $this->entityManager->flush();

        $this->competitionStageService->makeRoundRobinMatches($division);
        $this->divisionService->playDivisionMatches($division);
        $competition->addStage($division);

        $this->entityManager->persist($competition);
        $this->entityManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function runPlayoffs(CompetitionInterface $competition)
    {
        // for now solution only for 2 divisions
        if ($competition->getRules()->getDivisionCount() !== 2) {
            return;
        }
        $divisions = $this->getDivisions($competition);

        $rounds = $this->playoffService->createRoundsFromDivisions(
            $divisions->get(0),
            $divisions->get(1)
        );

        foreach ($rounds as $round) {
            $competition->addStage($round);
        }
        $this->playoffService->runAllRounds($rounds);
        $competition->setIsFinished(true);

        $this->entityManager->persist($competition);
        $this->entityManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getCompetitionResults(CompetitionInterface $competition): ArrayCollection {
        $results = $this->entityManager->getRepository(CompetitionResult::class)->findByCompetition($competition);
        if (!empty($results)) {
            return $results;
        }
        $playoffRounds = $this->getPlayoffRounds($competition);
        $wonMatches = [];
        foreach ($playoffRounds as $round) {
            foreach ($round->getMatches() as $match) {
                if ($match->getIsConsolation()) {
                    continue;
                }
                $winnerId = $match->getWinner()->getId();
                $score = $wonMatches[$winnerId] ?? 0;
                $wonMatches[$winnerId] = $score + 1;
            }
        }

        asort($wonMatches);
        $wonMatches = array_reverse($wonMatches, true);

        $standings = new ArrayCollection();
        $place = 1;
        foreach ($wonMatches as $id => $score) {
            $participant = $this->entityManager->getRepository(Participant::class)->findOneById($id);
            $result = new CompetitionResult($participant, $place);
            $standings->add($result);
            $this->entityManager->persist($result);
            $place++;
        }

        return $standings;
    }
}
