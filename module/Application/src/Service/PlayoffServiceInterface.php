<?php
namespace Application\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\CompetitionStageInterface;
use Application\Entity\PlayoffRoundInterface;

interface PlayoffServiceInterface
{
    /**
     * Will create matches for next round given the current round
     */
    public function createNextRoundMatches(PlayoffRoundInterface $round): ArrayCollection;

    /**
     * Will run all playoff stage rounds
     */
    public function runAllRounds(ArrayCollection $rounds);

    /**
     * Will create playoff stage rounds given finished division stages
     */
    public function createRoundsFromDivisions(CompetitionStageInterface $divisionOne, CompetitionStageInterface $divisionTwo): ArrayCollection;

    /**
     * Will seed initial playoff round, given finished division stages
     * TODO: is this needed?
     */
    public function initialRoundFromDivisionResults(CompetitionStageInterface $divisionOne, CompetitionStageInterface $divisionTwo): PlayoffRoundInterface;
}
