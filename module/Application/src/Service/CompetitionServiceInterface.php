<?php
namespace Application\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\CompetitionInterface;

interface CompetitionServiceInterface
{
    /**
     * Will create or get competition by id
     */
    public function getOrCreate(int $id): CompetitionInterface;

    /**
     * Will return division stages of the current competition
     */
    public function getDivisions(CompetitionInterface $competition): ArrayCollection;

    /**
     * Will return playoff stages of the current competition
     */
    public function getPlayoffRounds(CompetitionInterface $competition): ArrayCollection;

    /**
     * Will remove all division stages from the competition
     */
    public function removeDivisions(CompetitionInterface $competition);

    /**
     * Will run division stage given id
     */
    public function runDivisionStage(
        int $id,
        CompetitionInterface $competition,
        ArrayCollection $participants,
        array $options
    );

    /**
     * Will run playoffs for competition
     */
    public function runPlayoffs(CompetitionInterface $competition);

    /**
     * Will return competition results
     */
    public function getCompetitionResults(CompetitionInterface $competition): ArrayCollection;
}
