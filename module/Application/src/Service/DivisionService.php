<?php
namespace Application\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\CompetitionStageInterface;
use Application\Entity\CompetitionStage;
use Application\Entity\Division;
use Application\Entity\PlayoffRoundInterface;
use Application\Entity\PlayoffRound;
use Application\ValueObject\CompetitionStageRules;
use Application\Service\DivisionServiceInterface;
use Application\Entity\DivisionResultInterface;
use Application\Entity\DivisionResult;

class DivisionService implements DivisionServiceInterface
{
    /**
     * Entity manager.
     */
    private $entityManager;

    /**
     * Competition stage service
     */
    private $competitionStageService;

    public function __construct($entityManager, $competitionStageService)
    {
        $this->entityManager = $entityManager;
        $this->competitionStageService = $competitionStageService;
    }

    /**
     * {@inheritDoc}
     */
    public function playDivisionMatches(CompetitionStageInterface $division)
    {
        $this->competitionStageService->playStageMatches($division);
        $topResults = $this->getTopResults($division);

        $advances = new  ArrayCollection();
        foreach ($topResults as $result) {
            $advances->add($result->getParticipant());
        }

        $division->addAdvancedParticipants($advances);
        $division->setIsFinished(true);
    }

    /**
     * {@inheritDoc}
     */
    public function getTopResults(CompetitionStageInterface $stage): ArrayCollection
    {
        $results = $this->getResults($stage);
        $topResults = $results->getValues();
        // $resultIterator = $results->getIterator();
        uasort($topResults, function ($a, $b) {
            return $a->getPoints() < $b->getPoints();
        });
        $advancesCount = $stage->getRules()->getAdvancesCount();
        $topResults = array_slice($topResults, 0, $advancesCount);

        return new ArrayCollection($topResults);
    }

    /**
     * {@inheritDoc}
     */
    public function getResults(CompetitionStageInterface $stage): ArrayCollection
    {
        $results = new ArrayCollection();
        foreach ($stage->getMatches() as $match) {
            $points = 0;
            $winner = $match->getWinner();
            if ($results->containsKey($winner->getId())) {
                $points = $results->get($winner->getId())->getPoints();
            }
            $points += 1;
            $results->set(
                $winner->getId(),
                new DivisionResult($winner, $points)
            );
            $loser = $match->getLoser();
            if (! $results->containsKey($loser->getId())) {
                $results->set(
                    $loser->getId(),
                    new DivisionResult($loser, 0)
                );
            }
        }

        return $results;
    }
}
