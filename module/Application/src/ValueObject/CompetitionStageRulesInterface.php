<?php
namespace Application\ValueObject;

interface CompetitionStageRulesInterface
{
    /**
     * Will return the maximum allowed participants in this stage
     */
    public function getMaxParticipantCount(): int;

    /**
     * Will return number of participants that advance from this stage
     */
    public function getAdvancesCount(): int;

    /**
     * Will set if this is the last stage after which competition ends
     */
    public function setIsLast(bool $isLast);

    /**
     * Will get if this is the last stage after which competition ends
     */
    public function getIsLast(): bool;
}
