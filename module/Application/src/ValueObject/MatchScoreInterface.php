<?php
namespace Application\ValueObject;

use Application\Entity\ParticipantInterface;

interface MatchScoreInterface
{
    /**
     * return winner score
     */
    public function getWinnerScore(): int;

    /**
     * return loser score
     */
    public function getLoserScore(): int;
}
