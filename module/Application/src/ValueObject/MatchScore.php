<?php
namespace Application\ValueObject;

use Doctrine\ORM\Mapping as ORM;
use Application\ValueObject\MatchResultInterface;
use Application\Entity\ParticipantInterface;

use InvalidArgumentException;

/** @ORM\Embeddable */
class MatchScore implements MatchScoreInterface
{
    /**
     * @ORM\Column(name="winner_score", type="integer", nullable=true)
     */
    protected $winnerScore;

    /**
     * @ORM\Column(name="loser_score", type="integer", nullable=true)
     */
    protected $loserScore;

    public function __construct(int $winnerScore, int $loserScore)
    {
        if ($winnerScore < $loserScore) {
            throw new InvalidArgumentException(sprintf(
                "winner score %d can't be smaller than loser score %d",
                $winnerScore,
                $loserScore
            ));
        }
        $this->winnerScore = $winnerScore;
        $this->loserScore = $loserScore;
    }

    public function getWinnerScore(): int
    {
        return $this->winnerScore;
    }

    public function getLoserScore(): int
    {
        return $this->loserScore;
    }
}
