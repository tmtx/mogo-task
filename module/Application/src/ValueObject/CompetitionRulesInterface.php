<?php
namespace Application\ValueObject;

interface CompetitionRulesInterface
{
    /**
     * Will return the maximum allowed participants in this competition
     */
    public function getMaxParticipantCount(): int;

    /**
     * Will return division count in this competition
     */
    public function getDivisionCount(): int;
}
