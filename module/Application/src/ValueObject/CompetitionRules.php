<?php
namespace Application\ValueObject;

use Doctrine\ORM\Mapping as ORM;
use Application\ValueObject\CompetitionRulesInterface;

/** @ORM\Embeddable */
class CompetitionRules implements CompetitionRulesInterface
{
    /**
     * @ORM\Column(name="division_count", type="integer")
     */
    protected $divisionCount;

    /**
     * @ORM\Column(name="max_participant_count", type="integer")
     */
    protected $maxParticipantCount;

    public function __construct(array $rules)
    {
        $this->divisionCount = $rules['division_count'] ?? 2;
        $this->maxParticipantCount = $rules['max_participant_count'] ?? 20;
    }

    public function getMaxParticipantCount(): int
    {
        return $this->maxParticipantCount;
    }

    public function getDivisionCount(): int
    {
        return $this->divisionCount;
    }
}
