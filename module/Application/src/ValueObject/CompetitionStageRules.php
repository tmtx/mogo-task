<?php
namespace Application\ValueObject;

use Doctrine\ORM\Mapping as ORM;
use Application\ValueObject\CompetitionStageRulesInterface;

/** @ORM\Embeddable */
class CompetitionStageRules implements CompetitionStageRulesInterface
{
    /**
     * @ORM\Column(name="max_participant_count", type="integer")
     */
    protected $maxParticipantCount;

    /**
     * @ORM\Column(name="advances_count", type="integer")
     */
    protected $advancesCount;

    /**
     * @ORM\Column(name="is_last", type="boolean")
     */
    protected $isLast;

    public function __construct(array $rules)
    {
        $this->maxParticipantCount = $rules['max_participant_count'] ?? 20;
        $this->advancesCount = $rules['advances_count'] ?? 4;
        $this->isLast = $rules['last'] ?? false;
    }

    /**
     * {@inheritDoc}
     */
    public function getMaxParticipantCount(): int
    {
        return $this->maxParticipantCount;
    }

    /**
     * {@inheritDoc}
     */
    public function getAdvancesCount(): int
    {
        return $this->advancesCount;
    }

    /**
     * {@inheritDoc}
     */
    public function setIsLast(bool $isLast)
    {
        $this->isLast = $last;
    }

    /**
     * {@inheritDoc}
     */
    public function getIsLast(): bool
    {
        return $this->isLast;
    }
}
