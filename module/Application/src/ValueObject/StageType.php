<?php
namespace Application\ValueObject;

use MyCLabs\Enum\Enum;

use Doctrine\ORM\Mapping as ORM;

class StageType extends Enum
{
    private const NONE = 'none';
    private const DIVISION = 'division';
    private const PLAYOFF = 'playoff';
}
