# Mogo task

## Running

```bash
$ sudo docker-compose up
```

Wait for composer to install dependencies and apache to start.

After apache has started, solution will be available on http://localhost:9000
